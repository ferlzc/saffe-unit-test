#include <iostream>
#include <memory>
#include <string>
#include <exception>
#include "saffe_api.hpp"

using namespace SAFFE_API;

std::shared_ptr<SaffeApi> saffeEngine;

int main()
{
	std::string configPath = "C:\\libsaffe\\settings.json";
	//std::string configPath = "C:\\libsaffe";
	//std::string configPath = "C:\\libsaffe\\";

	try {
		saffeEngine = SaffeApi::get_saffe_api(configPath);
	}
	catch (SAFFE_API::SaffeApiException s_ex) {
		std::cout << s_ex.message << std::endl;
	}
	catch (std::exception& ex) {
		std::cout << "Exception: " << std::string(ex.what()) << std::endl;
	}
    std::cout << "Saffe Test!\n";
}